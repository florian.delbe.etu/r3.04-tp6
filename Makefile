SRC=src/main/java/
TST=src/test/java/
BIN=bin/
LIB=lib/

PACKAGE=fr.ulille.mvc
FOLDERS=$(subst .,/,$(PACKAGE))

JFXPATH=/home/public/javafx-sdk-17.0.2/lib
JFXFLAGS=--module-path $(JFXPATH) --add-modules javafx.controls,javafx.fxml
JFXEXEFLAGS=$(JFXFLAGS) -cp $(BIN)classes:.
JFXCLASS=$(PACKAGE).Mainfx

JUNITFLAGS=-d $(BIN)tests -cp $(LIB)junit-platform-console-standalone-1.10.0-M1.jar:$(BIN)classes:.
JUNITEXEFLAGS=-jar $(LIB)junit-platform-console-standalone-1.10.0-M1.jar execute --class-path=$(BIN)tests/:$(BIN)classes/ --scan-class-path --details=verbose

JVM=java
JC=javac
JFLAGS=-d $(BIN)classes
MAINCLASS=$(PACKAGE).Main

all: compilation
	$(JVM) $(JFXEXEFLAGS) $(MAINCLASS)

gui: compilation
	$(JVM) $(JFXEXEFLAGS) $(JFXCLASS)

test: compilation
	$(JC) $(JUNITFLAGS) $(TST)$(FOLDERS)/*/*/*.java
	$(JVM) $(JUNITEXEFLAGS)

compilation: $(BIN)
	$(JC) $(JFLAGS) $(JFXFLAGS) $(SRC)$(FOLDERS)/*.java $(SRC)$(FOLDERS)/*/*.java
$(BIN):
	mkdir -p $(BIN)tests $(BIN)classes

clean: $(BIN)
	rm -r $(BIN)tests/* $(BIN)classes/* 
