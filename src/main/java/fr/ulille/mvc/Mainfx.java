package fr.ulille.mvc;

import fr.ulille.mvc.utils.*;
import fr.ulille.mvc.model.*;
import fr.ulille.mvc.view.*;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Mainfx extends Application  {
    private Label label1, label2, label3;
    private IDice die1, die2, die3;
    
    @Override
    public void start(Stage arg0) throws Exception {
	this.label1 = new Label("");
	this.label2 = new Label("");
	this.label3 = new Label("");

	this.die1 = new Dice(6);
	this.die2 = new Dice(10);
	this.die3 = new Modifier('+', new Modifier('*', new Dice(4), 7), 2000);

        Button b1 = new Button("lancer dé " + this.die1.getBound());
	b1.setOnAction((e) -> die1.roll());
	Button b2 = new Button("lancer dé " + this.die2.getBound());
	b2.setOnAction((e) -> die2.roll());
	Button b3 = new Button("lancer dé " + this.die3.getBound());
	b3.setOnAction((e) -> die3.roll());
	Button b4 = new Button("lancer tout les dés");
	b4.setOnAction((e) -> {
		die1.roll();
		die2.roll();
		die3.roll();
	    });

	// associe un label à un dé pour isoler les affichages
	new DiceViewer(die1, label1);
	new DiceViewer(die2, label2);
	new DiceViewer(die3, label3);
	
	VBox vbox1 = new VBox(b1, label1);
	VBox vbox2 = new VBox(b2, label2);
	VBox vbox3 = new VBox(b3, label3);
	HBox hbox = new HBox(vbox1, vbox2, vbox3);
	VBox vbox = new VBox(hbox, b4);
	
	Scene scene = new Scene(vbox);

	arg0.setScene(scene);
        arg0.setTitle("D de MORT");
        arg0.setHeight(150);
        arg0.setWidth(300);
        arg0.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
