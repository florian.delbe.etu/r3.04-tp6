package fr.ulille.mvc.model;

import java.util.Random;

import fr.ulille.mvc.utils.Subject;

public class Dice extends Subject implements IDice {
    protected int bound, value;
    private Random alea;

    public Dice(int bound) {
        this.bound = bound;
        this.alea = new Random();
    }

    public int getBound() {
        return this.bound;
    }

    public int getValue() {
        return this.value;
    }

    public void roll() {
        this.value = alea.nextInt(bound);
        this.notifyObservers();
    }
}
