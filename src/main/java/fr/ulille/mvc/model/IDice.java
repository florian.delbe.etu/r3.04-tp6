package fr.ulille.mvc.model;

public interface IDice {
    // rend la valeur maximale accessible
    public int getBound();
    // rend la dernière valeur tirée
    public int getValue();
    // mise à jour de la valeur du dé [0 - maxValue]
    public void roll();
}
