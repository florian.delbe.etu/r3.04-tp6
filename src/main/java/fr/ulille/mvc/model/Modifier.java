package fr.ulille.mvc.model;
import fr.ulille.mvc.utils.Observer;
import fr.ulille.mvc.utils.Subject;
public class Modifier extends Subject implements IDice {
    private char type;
    private int modifier;
    private IDice dice;

    public Modifier(char type, IDice dice, int modifier) {
	this.type = type;
	this.modifier = modifier;
	this.dice = dice;
    }
    
    public int getBound() {
        return this.dice.getBound();
    }

    public int getValue() {
	int value = this.dice.getValue();
	switch (type) {
            case '*':
                value *= modifier;
                break;
            case '-':
                value -= modifier;
                break;
            case '+':
                value += modifier;
		break;
            default:
                break;
        }
        return value;
    }

    public void attach(Observer obs) {
	((Subject)this.dice).attach(obs);
    }

    public void roll() {
	this.dice.roll();
    }
}
