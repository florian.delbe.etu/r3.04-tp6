package fr.ulille.mvc.model;

public class SpecialDice extends Dice {
    private int modifierValue;
    private char modifierType;

    public SpecialDice(int bound) {
        super(bound);

    }

    public SpecialDice(int bound, int modifierValue, char modifierType) {
        super(bound);
        this.modifierType = modifierType;
        this.modifierValue = modifierValue;
    }

    @Override
    public void roll() {
        super.roll();
        switch (modifierType) {
            case '*':
                this.value *= modifierValue;
                break;
            case '-':
                this.value -= modifierValue;
                break;
            case '+':
                this.value += modifierValue;
            default:
                break;
        }

    }
}
