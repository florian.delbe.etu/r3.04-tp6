package fr.ulille.mvc.utils;

public interface Observer {
    public void update(Subject subj);

    public void update(Subject subj, Object data);
}
