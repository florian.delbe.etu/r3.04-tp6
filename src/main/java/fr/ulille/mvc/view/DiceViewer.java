package fr.ulille.mvc.view;

import javafx.scene.control.Label;

import fr.ulille.mvc.utils.*;
import fr.ulille.mvc.model.*;

public class DiceViewer implements Observer {
    private IDice dice;
    private Label label;
    
    public DiceViewer(IDice dice, Label label) {
	this.dice = dice;
	this.label = label;
	((Subject) dice).attach(this);
    }

    @Override
    public void update(Subject subj) {
	label.setText("" + dice.getValue());
    }

    @Override
    public void update(Subject subj, Object data) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

}
